//JS OBJECTS

/*
	Objects -> is a collection of related data and/or functionalities. The purpose of an object is not only to store multiple data sets but also represent real-world objects.

	Syntax: 
		let / const variableName = {
			key/property: value
		}

	Note: Information stored in objects are represented in key:value pairing.
*/

	// 'key' -> is also mostly referred to as a 'property' of an object.

	//Example:
	let cellphone = {
		name: 'Nokia 3210',
		manufactureDate: '1999',
		price: 1000
	};

	console.log(cellphone); // {name: 'Nokia 3210', manufactureDate: '1999', price: 1000}
	console.log(typeof cellphone); //object

	// How to store multiple objects?

	// We can use an array structure to store them.
	let users = [
		{ 	
			name: 'Anna', 
		 	age: '23'
		},
		{ 	
		 	name: 'Nicole', 
		 	age: '18'
		},	
		{ 	
		 	name: 'Smith', 
			age: '42'
		},	
		{ 	
			name: 'Pam', 
			age: '13'
		},	
		{ 	
			name: 'Anderson', 
			age: '26'
		}	
	];

	console.log(users); // [{ name: 'Anna', age: '23' }, { name: 'Nicole', age: '18' }, { name: 'Smith', age: '42' }, { name: 'Pam', age: '13' }, { name: 'Anderson', age: '26' }]

	// There is an alternative way of displaying values of an object collection
	console.table(users); //tabular display

/*	Complex examples of JS Objects

	NOTE: Different data types may be stored in an object's property creating more complex data structures

	When using JS Objects, we can insert 'methods'.

	Methods -> are useful for creating reusable functions that can perform tasks related to an object.
*/

	let friend = {
		//properties
		firstName: 'Joe', //string or numbers
		lastName: 'Smith',
		isSingle: true, //boolean
		emails: ['joesmith@gmail.com', 'jsmith@company.com'], //array
		address: {  // objects
			city: 'Austin',
			state: 'Texas',
			country: 'USA'
		},
		//methods
		introduce: function() {
			console.log('Hello, meet my new friend');
		},
		advice: function() {
			console.log('Give friend an advice to move on');
		},
		wingman: function() {
			console.log('Hype friend when meeting new people');
		}
	};
	console.log(friend); //{firstName: 'Joe', lastName: 'Smith', isSingle: true, emails: ['joesmith@gmail.com', 'jsmith@company.com'], address: { city: 'Austin', state: 'Texas', country: 'USA' }, introduce: [Function: introduce], advice: [Function: advice], wingman: [Function: wingman]}


// ALTERNATE APPROACHES ON HOW TO DECLARE AN OBJECT IN JAVASCRIPT

/*	
	1. How to create an Object using a Constructor function?

	Constructor function -> we'll need to create a reusable function in which we will declare a blueprint to describe the anatomy of an object that we wish to create.

	To declare properties within a constructor function, you have to be familiar with the 'this' keywpord

	Benefits/Usefulness: Constructor function is useful when creating instance of several object that have the same data structures.

	'instance' -> this refers to a concrete occurrence of any object which emphasizes on the distinct/unique identity of the object/subject.
*/

/*	'this' is NOT a variable, but a keyword.
		this -> refers to the object in this example, 'this' refers to the 'global' object *Laptop* because the this keyword is within the scope of the laptop constructor function.

		To create a new object that has the properties stated above, we will use the 'new' keyword for us to be able to create a new instance of an object.
*/

	//Example 1:
	function Laptop(name, year, model) {
		this.brand = name;
		this.manufacturedOn = year;
		this.model = model;
	};

	let laptop1 = new Laptop('Sony', 2008, 'Viao');
	let laptop2 = new Laptop('Apple', 2019, 'Mac');
	let laptop3 = new Laptop('HP', 2015, 'hp');

	console.log(laptop1); //Laptop {brand: 'Sony', manufacturedOn: 2008, model: 'Viao'}
	console.log(laptop2); //Laptop {brand: 'Apple', manufacturedOn: 2019, model: 'Mac'}


	//What if I would want to store them in a single container and display them in a tabular format?

	let gadgets = [laptop1, laptop2, laptop3];
	console.table(gadgets); //tabular display


	//Example 2:
	function Pokemon(name, type, level, trainer) {
		//properties
		this.pokemonName = name;
		this.pokemonType = type;
		this.pokemonHealth = 2 * level;
		this.pokemonLevel = level;
		this.owner = trainer;
		//methods
		this.tackle = function(target) {
			console.log(`${this.pokemonName} tackled ${target.pokemonName}`);
		};
		this.greetings = function() {
			console.log(`${this.pokemonName} says Hello!`);
		};
	};

	let pikachu = new Pokemon('Pikachu', 'electric', 3, 'Ash Ketchup');
	let ratata = new Pokemon('Ratata', 'normal', 4, 'Misty');
	let snorlax = new Pokemon('Snorlax', 'normal', 5, 'Gary Oak');
	let pokemons = [pikachu, ratata, snorlax];
	console.table(pokemons); //tabular display


// ACCESS OBJECT PROPERTIES

/*	
	1. DOT NOTATION
	Using the (.) dot notation, we can access properties of an object.

	Syntax: objectName.propertyName
*/

	console.log(snorlax.owner); //Gary Oak
	console.log(pikachu.pokemonType); //electric

	//accessing a method of an object
	pikachu.tackle(ratata); //Pikachu tackled Ratata
	snorlax.greetings(); //Snorlax says Hello!

/*
	2. SQUARE BRACKETS 
		-> an alternative approach for accessing object properties.

	Syntax: objectName['property name']

*/
	console.log(ratata['owner']); //Misty
	console.log(ratata['pokemonLevel']); //4
	// It will return undefined if you try to access a non-existing property.

/*
	Which is the BEST USE CASE: dot notation or square bracket?

	We will stick with dot notations when accessing properties of an object, as our convention when managing properties of an object.

	We will use square brackets when dealing with indexes of an array.
*/

//[ADDITIONAL KNOWLEDGE]
/*
	We can write and declare objects this way:

	To create objects, you will use object literals '{}'
*/
	let trainer = {}; //empty object
	console.log(trainer); //{}

	// Will I be able to add properties from outside of an object? YES

	trainer.name = 'Ash Ketchum';
	console.log(trainer); //{name: 'Ash Ketchum'}
	trainer.friends = ['Misty', 'Brock', 'Tracey'];
	console.log(trainer); //{name: 'Ash Ketchum', friends: ['Misty', 'Brock', 'Tracey']}


	//method
	trainer.speak = function() {
		console.log('Pikachu, I choose you!'); 
	};

	trainer.speak(); //Pikachu, I choose you!
	//possible approach